package br.com.iteris.coins;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by rasc on 12/07/2018.
 */

public interface CoinService {
    @GET("ticker")
    Call<List<Coin>> getCoin();
}
