package br.com.iteris.coins;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class CoinAdapter extends RecyclerView.Adapter<CoinAdapter.CoinHolder> {

    private Context context;
    private List<Coin> coins;
    private LayoutInflater layoutInflater;

    public CoinAdapter(Context context, List<Coin> coins) {
        this.context = context;
        this.coins = coins;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public CoinHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.adapter_coin, parent, false);
        return new CoinHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CoinHolder holder, int position) {
        Coin coin = coins.get(position);
        Glide.with(context)
                .load("https://res.cloudinary.com/dxi90ksom/image/upload/" + coin.getSymbol() + ".png")
                .into(holder.ivCoin);
        holder.tvSymbol.setText(coin.getSymbol());
        holder.tvName.setText(coin.getName());
        holder.tvPriceUsd.setText(context.getString(R.string.dollar, coin.getPriceUsd()));
        holder.tvPercentChange1h.setText(context.getString(R.string.percent, coin.getPercentChange1h()));
        holder.tvPercentChange24h.setText(context.getString(R.string.percent, coin.getPercentChange24h()));
        holder.tvPercentChange7d.setText(context.getString(R.string.percent, coin.getPercentChange7d()));
        changeColor(holder.tvPercentChange1h, coin.getPercentChange1h());
        changeColor(holder.tvPercentChange24h, coin.getPercentChange24h());
        changeColor(holder.tvPercentChange7d, coin.getPercentChange7d());
    }

    private void changeColor(TextView textView, String value) {
        if (value.contains("-")) {
            textView.setTextColor(Color.RED);
            return;
        }
        textView.setTextColor(Color.parseColor("#32CD32"));
    }

    @Override
    public int getItemCount() {
        return coins.size();
    }

    public static class CoinHolder extends RecyclerView.ViewHolder {
        ImageView ivCoin;
        TextView tvSymbol;
        TextView tvName;
        TextView tvPriceUsd;
        TextView tvPercentChange1h;
        TextView tvPercentChange24h;
        TextView tvPercentChange7d;

        public CoinHolder(View itemView) {
            super(itemView);
            ivCoin = itemView.findViewById(R.id.iv_coin);
            tvSymbol = itemView.findViewById(R.id.tv_symbol);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPriceUsd = itemView.findViewById(R.id.tv_price_usd);
            tvPercentChange1h = itemView.findViewById(R.id.tv_percent_change_1h);
            tvPercentChange24h = itemView.findViewById(R.id.tv_percent_change_24h);
            tvPercentChange7d = itemView.findViewById(R.id.tv_percent_change_7d);
        }
    }
}
